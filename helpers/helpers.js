export const isCreditcardNumberValid = number => {
  const numbers = number
    .toString()
    .replace(/\s+/g, "")
    .split("")
    .map(Number);
  if (
    Number.isNaN(number) ||
    number == null ||
    number == "" ||
    numbers.length != 16
  ) {
    return false;
  }

  const sum = numbers
    .map((number, index) => {
      if (index % 2 == 0) {
        let doubled = number * 2;
        if (doubled > 9) {
          doubled = doubled - 9;
        }
        return doubled;
      }
      return number;
    })
    .reduce((total, curr) => {
      return total + curr;
    });
  let lastNumber = sum
    .toString()
    .split("")
    .map(Number);
  return lastNumber[lastNumber.length - 1] == 0;
};

export const creditcardProviders = number => {
  if (Number.isNaN(number) || number == null || number == "") {
    return [];
  }
  number = number.toString().replace(/\s+/g, "");
  const providers = {
    visa: "^4[0-9]{12}(?:[0-9]{3})?$",
    mastercard: "^5[1-5][0-9]{14}$",
    amex: "^3[47][0-9]{13}$",
    maestro: "^(5018|5020|5038|6304|6759|6761|6763)[0-9]{8,15}$"
  };

  const matchedProviders = [];
  for (const [key, value] of Object.entries(providers)) {
    const regex = new RegExp(value);
    if (regex.test(number)) {
      matchedProviders.push(key);
    }
  }
  return matchedProviders;
};
